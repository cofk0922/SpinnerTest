package com.github.cofk0922.spinnertest;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class SpinnerActivity extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private String facebook, google, myspace, gmail, xtra;

    String[] web_array;

    String selectedText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        // declare the items as TAG so we can know which item is selected
        facebook = getString(R.string.facebook);
        google = getString(R.string.google);
        myspace = getString(R.string.myspace);
        gmail = getString(R.string.gmail);
        xtra = getString(R.string.xtra);

        // declare the item array so we can put in spinner adapter
        web_array = getResources().getStringArray(R.array.websites);

        // declare spinner and set spinner adapter and onItemSelected text
        Spinner spinner = findViewById(R.id.spinner);
        spinner.setAdapter(new ArrayAdapter<>(
                SpinnerActivity.this,
                android.R.layout.simple_spinner_item,
                web_array
        ));
        spinner.setOnItemSelectedListener(this);

        // search button click event
        findViewById(R.id.btn_search).setOnClickListener(this);
    }

    // The button click event
    @Override
    public void onClick(View view) {

        // search button event
        String url;
        // see what text is selected, and where url need to go
        if (selectedText.equals(facebook)) {
            url = "https://www.facebook.com/";
        } else if (selectedText.equals(google)) {
            url = "https://www.google.com/";
        } else if (selectedText.equals(myspace)) {
            url = "https://myspace.com/";
        } else if (selectedText.equals(gmail)) {
            url = "https://mail.google.com/mail/?tab=wm";
        } else if (selectedText.equals(xtra)) {
            url = "http://www.xtra.tw/index.php?lang=en";
        } else {
            url = "";
        }

        if (url.equals("")) return;

        // intent to user default browser, and open the url
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    // The spinner catch onItemSelected event
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // this is used to catch what item is selected
        selectedText = (String) adapterView.getSelectedItem();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        // this is the default item if user no select any item
        selectedText = (String) adapterView.getItemAtPosition(0);
    }
}
